---
# motorScan

-- in toydaq folder:

- git clone https://gitlab.com/dfulla/motorscan

- cd motorscan/src

# option real time plot:

- python gui_scan.py
- Uses scan_thread

# option static plot:

- python fixed_scan.py
- Uses scan_iter

# option camera display:

- python camera_plot.py


## Name

- motorscan

## Description

- User inputs values of start, stop and step size
- Selects one motor, out of three
- Selects the diodes [0-3]
- presses GO! and gets the plots

## Visuals

## Installation

## Usage

## Support

- in case of doubt contact Daniel Fulla:
- fulla75@msn.com

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Authors and acknowledgment

## License

## Tag rules:

- tag: A.B.C - D.E.F
- A: Major change of the code structure (re-writing, re-structuring etc)
- B: New functionalities 
- C: Small fixes (bugs etc)
- D,E,F refers to the version of the control system (e.g. 7.0.7)

## Project status

- Work in progress

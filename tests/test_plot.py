import unittest
import sys
sys.path.append('../src/')
from scan_motor_diodes import exec_diode
from scan_motor_diodes import fix_list


# The test case
class TestExecDiode(unittest.TestCase):

    def test_empty_list(self):
        empty_result = exec_diode([])
        self.assertEqual(empty_result, [])


class TestFixList(unittest.TestCase):

    def testCase1(self):
        list_diodes = ['INTENSITY']
        result = fix_list(list_diodes)
        self.assertEqual(result, ['INTENSITY'])

    def testCase2(self):
        list_diodes = ['INTENSITY', 'SIGNAL']
        result = fix_list(list_diodes)
        self.assertEqual(result, ['INTENSITY', 'SIGNAL'])

    def testCase3(self):
        list_diodes = ['INTENSITY', 'SIGNAL', 'COUNTER']
        result = fix_list(list_diodes)
        self.assertEqual(result, ['INTENSITY', 'SIGNAL', 'COUNTER'])


if __name__ == '__main__':
    unittest.main()

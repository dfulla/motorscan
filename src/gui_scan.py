from matplotlib.backends.backend_qt5agg import (
    FigureCanvasQTAgg as FigureCanvas
)
from matplotlib.figure import Figure
from PyQt5.QtWidgets import (QGroupBox, QApplication, QMainWindow, QWidget,
                             QVBoxLayout, QLabel, QLineEdit, QPushButton,
                             QCheckBox, QHBoxLayout, QComboBox)
import sys
sys.path.append('../../')
from scan_motor_diodes import plotRealTime


class MainWindow(QMainWindow):

    '''
    This GUI collects the input parameters to perform a scan
    Imports the plotting file and passes the input parameters as arguments
    '''

    def __init__(self):
        super().__init__()
        self.setWindowTitle("Diode values vs motor positions")
        self.setGeometry(100, 100, 800, 600)
        self.setup_ui()

    def setup_ui(self):
        main_widget = QWidget()
        main_layout = QVBoxLayout()

        # Motor selection
        motor_layout = QHBoxLayout()
        self.motor_label = QLabel("Selected Motor:", self)
        self.motor_box = QComboBox()
        self.motor_box.addItem("MY-LARGE-MOTOR")
        self.motor_box.addItem("MY-TINY-MOTOR")
        self.motor_box.addItem("MY-NORMAL-MOTOR")

        self.motor_box.currentIndexChanged.connect(self.update_motor_label)
        motor_layout.addWidget(self.motor_label)
        motor_layout.addWidget(self.motor_box)
        main_layout.addLayout(motor_layout)

        # Checkbox inputs
        diode_label = QLabel("Diodes:")
        diode_layout = QVBoxLayout()
        self.intensity = QCheckBox("INTENSITY", self)
        self.counter = QCheckBox("COUNTER", self)
        self.signal = QCheckBox("SIGNAL", self)

        diode_layout.addWidget(diode_label)
        diode_layout.addWidget(self.intensity)
        diode_layout.addWidget(self.counter)
        diode_layout.addWidget(self.signal)

        # Create checkbox group box
        checkbox_group_box = QGroupBox()
        checkbox_group_box.setLayout(diode_layout)
        # Create rectangle with a darker grey color
        setup_rectangle = """
        QGroupBox { background-color: #BBBBBB;
                    border: 1px solid #555555;
        }"""
        checkbox_group_box.setStyleSheet(setup_rectangle)
        # Add checkbox group box to main layout
        main_layout.addWidget(checkbox_group_box)

        # Start, stop, step size inputs
        motor_inputs_layout = QHBoxLayout()
        self.start_label = QLabel("Start:")
        self.start_input = QLineEdit()
        self.stop_label = QLabel("Stop:")
        self.stop_input = QLineEdit()
        self.step_size_label = QLabel("Step Size")
        self.step_size_input = QLineEdit()
        motor_inputs_layout.addWidget(self.start_label)
        motor_inputs_layout.addWidget(self.start_input)
        motor_inputs_layout.addWidget(self.stop_label)
        motor_inputs_layout.addWidget(self.stop_input)
        motor_inputs_layout.addWidget(self.step_size_label)
        motor_inputs_layout.addWidget(self.step_size_input)
        main_layout.addLayout(motor_inputs_layout)

        # Plot button
        plot_button = QPushButton("GO!")
        setup_button = """
            background-color: #333333;
            color: white;
            font-size: 16px;
        """
        plot_button.setStyleSheet(setup_button)
        plot_button.clicked.connect(self.plot_scan)
        main_layout.addWidget(plot_button)

        # Figure canvas
        self.figure = Figure()
        self.canvas = FigureCanvas(self.figure)
        main_layout.addWidget(self.canvas)

        # Set main widget layout and show window
        main_widget.setLayout(main_layout)
        self.setCentralWidget(main_widget)
        self.show()

    def validate(self, start, stop, step_size):

        if stop <= start:
            print('stop should have a greater value than start.')
            return -1

        return start, stop, step_size

    def update_motor_label(self):

        motor = self.motor_box.currentText()
        self.motor_label.setText("Selected Motor: " + motor)

    def inputs2plot(self):

        motor_name = self.motor_box.currentText()
        start = float(self.start_input.text())
        stop = float(self.stop_input.text())
        step_size = float(self.step_size_input.text())
        diodes = [self.intensity, self.counter, self.signal]
        diodes_checked = []
        for diode in diodes:

            if diode.isChecked():

                diodes_checked.append(diode.text())

        return motor_name, start, stop, step_size, list(diodes_checked)

    def plot_scan(self):

        motor_name, start, stop, step_size, diodes_checked = self.inputs2plot()
        if self.validate(start, stop, step_size) != -1:
            plotRealTime(motor_name, start, stop, step_size, diodes_checked)
        else:
            print('Check input. Parameters not accepted')


if __name__ == '__main__':

    app = QApplication(sys.argv)
    window = MainWindow()
    sys.exit(app.exec_())

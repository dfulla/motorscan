from matplotlib import pyplot as plt
import matplotlib.animation as animation
import sys
sys.path.append('../../')
from toydaq import Motor, Camera, scan_iter

class ImageAnimation:
    def __init__(self, motor_name, n_rows=100, n_cols=100, milliseconds=100):
        self.motor_name = motor_name
        self.mot = Motor(motor_name, units="mm")
        self.cam = Camera("MY-CAMERA")
        self.n_rows = n_rows
        self.n_cols = n_cols
        self.milliseconds = milliseconds
        self.fig, self.ax = plt.subplots()
        self.img = self.cam.get()
        self.pims = plt.imshow(self.img)
        self.ani = animation.FuncAnimation(self.fig,
                                           self.update_image,
                                           frames=10,
                                           interval=self.milliseconds,
                                           blit=True)

    def update_image(self, frame):
        img = self.cam.get()
        self.pims.set_array(img)
        new_image = img
        self.pims.set_data(new_image)
        for x in scan_iter(self.mot, frame, frame+1, 1):
            plt.title(f'{self.motor_name} at {x:.3f}')
        return self.pims,

    def show(self):
        plt.show()


animation_obj = ImageAnimation("MY-MOTOR")
animation_obj.show()

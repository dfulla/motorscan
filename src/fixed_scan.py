import matplotlib.pyplot as plt
from PyQt5.QtWidgets import (QGroupBox, QApplication, QMainWindow,
                             QWidget, QVBoxLayout, QLabel, QLineEdit,
                             QPushButton, QCheckBox, QHBoxLayout,
                             QComboBox)
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
import numpy as np

import sys
sys.path.append('../../')
from toydaq import Motor, Diode, scan_iter


class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("Function Plotter")
        self.setGeometry(100, 100, 800, 600)
        self.setup_ui()

    def setup_ui(self):
        main_widget = QWidget()
        main_layout = QVBoxLayout()

        # Motor selection
        func_layout = QHBoxLayout()
        self.func_label = QLabel("Selected Motor:", self)
        self.func_box = QComboBox()
        self.func_box.addItem("MY-LARGE-MOTOR")
        self.func_box.addItem("MY-TINY-MOTOR")
        self.func_box.addItem("MY-NORMAL-MOTOR")
        self.func_box.currentIndexChanged.connect(self.update_function_label)

        func_layout.addWidget(self.func_label)
        func_layout.addWidget(self.func_box)
        main_layout.addLayout(func_layout)

        # Checkbox inputs
        diode_label = QLabel("Diodes:")
        diode_layout = QVBoxLayout()
        self.intensity = QCheckBox("INTENSITY", self)
        self.counter = QCheckBox("COUNTER", self)
        self.signal = QCheckBox("SIGNAL", self)
        diode_layout.addWidget(diode_label)
        diode_layout.addWidget(self.intensity)
        diode_layout.addWidget(self.counter)
        diode_layout.addWidget(self.signal)

        # Create checkbox group box
        checkbox_group_box = QGroupBox()
        checkbox_group_box.setLayout(diode_layout)

        # Create rectangle with a darker grey color
        checkbox_group_box.setStyleSheet("""
        QGroupBox { background-color: #BBBBBB;
                    border: 1px solid #555555;
        }""")

        # Add checkbox group box to main layout
        main_layout.addWidget(checkbox_group_box)

        # Start, Stop, Step size inputs
        freq_phase_layout = QHBoxLayout()
        self.start_label = QLabel("Start:")
        self.start_input = QLineEdit()
        self.stop_label = QLabel("Stop:")
        self.stop_input = QLineEdit()
        self.step_size_label = QLabel("Step Size")
        self.step_size_input = QLineEdit()
        freq_phase_layout.addWidget(self.start_label)
        freq_phase_layout.addWidget(self.start_input)
        freq_phase_layout.addWidget(self.stop_label)
        freq_phase_layout.addWidget(self.stop_input)
        freq_phase_layout.addWidget(self.step_size_label)
        freq_phase_layout.addWidget(self.step_size_input)
        main_layout.addLayout(freq_phase_layout)

        # Plot button
        plot_button = QPushButton("GO!")
        plot_button.setStyleSheet("background-color: #333333; color: white;")
        plot_button.clicked.connect(self.plot_function)
        main_layout.addWidget(plot_button)

        # Figure canvas
        self.figure = Figure()
        self.canvas = FigureCanvas(self.figure)
        main_layout.addWidget(self.canvas)

        # Set main widget layout and show window
        main_widget.setLayout(main_layout)
        self.setCentralWidget(main_widget)
        self.show()

    def update_function_label(self):

        self.func_label.setText("Selected Function: " + self.func_box.currentText())

    def getScan(self, motor, diode):

        x1 = []
        y1 = []
        start = self.start_input.text()
        stop = self.stop_input.text()
        step_size = self.step_size_input.text()
        start, stop, step_size = self.validate(start, stop, step_size)
        motor = Motor(motor, step_size, units="m")
        dio = Diode(diode)

        for pos in scan_iter(motor, start, stop, step_size):
            print(pos, dio.get())
            x1.append(pos)
            y1.append(dio.get())

        print(x1, y1)
        return x1, y1

    def validate(self, start, stop, step_size):
        print(start, stop, step_size)
        return float(start), float(stop), float(step_size)

    def plot_function(self):
        plt.ion()
        motor = self.func_box.currentText()
        diodes = [self.intensity, self.counter, self.signal]

        self.figure.clear()
        ax = self.figure.add_subplot(111)
        for diode in diodes:
            if diode.isChecked():
                x1, y1 = self.getScan(motor, diode.text())
                ax.plot(x1, y1, label=diode.text())
                self.show()
                ax.title.set_text(motor)
                ax.legend()
                self.canvas.draw()

        ax.title.set_text(motor)
        ax.legend()
        self.canvas.draw()

    def update_plot(self):
        self.y = np.roll(self.y, 1)
        self.ax.lines[0].set_ydata(self.y)
        self.canvas.draw()

    def initUI(self):
        self.setWindowTitle('Real-time Scan Plotting')
        self.show()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = MainWindow()
    sys.exit(app.exec_())

import matplotlib.pyplot as plt
import sys
sys.path.append('../../')
sys.path.append('../config')
from devices_dict import motors_dict, colors, markers, diodes
from toydaq import Motor, Diode, scan_thread


def plotRealTime(motor_name, start, stop, step_size, list_diodes, speed=10):

    '''
    Real time update plot. Diode values vs motor position
    example of input:
    plotRealTime('MY-LARGE-MOTOR', 0, 10, 1, diodes)
    diodes is a list of Diode instances of each diode
    '''

    motor_unit = motors_dict[motor_name]['units']
    motor = Motor(motor_name, step_size, speed, units=motor_unit)
    # motor = Motor(motor_name, units=motor_unit, step_size=step_size)
    diodes = exec_diode(list_diodes)
    st = scan_thread(motor, start, stop, step_size)

    plt.ion()
    fig, ax = plt.subplots()
    plt.title(f'{motor_name}', fontsize=14)
    plt.xlabel(f'motor distance [{motor_unit}]', fontsize=14)
    plt.ylabel('diode values', fontsize=14)
    counter = 0

    while st.is_alive():

        motor_position = float(str(motor).split(' ')[-2])
        for i, diode in enumerate(diodes):
            value_diode = float(str(diode).split(' ')[-1])
            if counter < len(diodes):
                name_diode = str(diode).split(' ')[1]
                plt.scatter(motor_position,
                            value_diode,
                            marker=markers[i],
                            c=colors[i],
                            zorder=1,
                            label=f'{name_diode}')
            else:
                plt.scatter(motor_position, value_diode,
                            marker=markers[i],
                            c=colors[i],
                            zorder=1)
            counter += 1

        plt.legend()
        plt.show()
        plt.pause(0.1)

        if not st.is_alive():
            plt.show(block=True)


def exec_diode(list_diodes: list) -> list:

    if len(list_diodes) != 0:
        diodes = []
        for diode in list_diodes:
            diodes.append(Diode(diode))
    else:
        return []
    return diodes


def fix_list(list_diodes: list) -> list:

    if list_diodes[0] == '[' and list_diodes[-1] == ']':
        list_diodes = list_diodes[1:-1].split(',')
    else:
        pass

    return list_diodes


def input_variables():

        motor_name = sys.argv[1]
        unit = motors_dict[motor_name]['units']
        start = float(sys.argv[2])
        stop = float(sys.argv[3])
        step_size = float(sys.argv[4])
        list_diodes = fix_list(sys.argv[5])
        diodes = exec_diode(list_diodes)
        return motor_name, start, stop, step_size, diodes

def speed_input():
    try:
        speed = int(sys.argv[-1])
        return speed
    except:
        return None

if __name__ == '__main__':

    '''
    Example to execute from terminal:
    python scan_motor_diodes.py 'MY-NORMAL-MOTOR' 0 10 1 '["INTENSITY"]'
    '''
    # fix the length:
    if len(sys.argv) >= 6:

        motor_name, start, stop, step_size, diodes = input_variables()
        try:
            speed = speed_input()
            
            if speed:
                plotRealTime(motor_name, start, stop, step_size, diodes, speed)
            else:
                plotRealTime(motor_name, start, stop, step_size, diodes)
        except:
            print('Include step_size in class Motor in devices')

    if len(sys.argv) == 1:
        print('Provide input paramenters e.g.:')
        execution = "python scan_motor_diodes.py 'MY-NORMAL-MOTOR'"
        print(f"{execution} 0 10 1 '['INTENSITY', 'SIGNAL']'")


    # plotRealTime('MY-LARGE-MOTOR', 0, 10, 1, diodes)

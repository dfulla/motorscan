#!/usr/bin/env python3

import sys
sys.path.append('../../')
from toydaq import Diode

motors_dict = {
    'MY-LARGE-MOTOR': {'motor': "MY-LARGE-MOTOR",  'units': "m"},
    'MY-NORMAL-MOTOR': {'motor': "MY-NORMAL-MOTOR",  'units': "mm"},
    'MY-TINY-MOTOR': {'motor': "MY-TINY-MOTOR",  'units': "nm"},
    }

diodes_dict = {
    'diode1': "INTENSITY",
    'diode2': "COUNTER",
    'diode3': "SIGNAL",
    }


color1 = 'blue'
color2 = 'red'
color3 = 'black'
color4 = 'black'

marker1 = 'o'
marker2 = 'x'
marker3 = '>'
marker4 = '<'

colors = [color1, color2, color3, color4]
markers = [marker1, marker2, marker3, marker4]

dio4 = Diode("MY-DIODE")

dio1 = Diode("INTENSITY")
dio2 = Diode("COUNTER")
dio3 = Diode("SIGNAL")

diodes = [dio1, dio2, dio3, dio4]
